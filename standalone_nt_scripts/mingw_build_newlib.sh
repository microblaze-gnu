
#  Build Newlib
#
LANGS="c,c++"
CFLAGS="-g -O2"
CXXFLAGS="-g -O2"

. mingw_common.sh
#. ./common_eager.sh
PATH=$CURDIR/build/tools/bin:$PATH
GCC_SRCDIR=$SRCDIR/gcc
TARGET=microblaze-xilinx-elf
SYSROOT=$RELDIR/$TARGET

# Override source directory
SRCDIR=$SRCDIR/newlib

BLDDIR=$BLDDIR/bld_newlib
BINDIR=$RELDIR/bin

DESC="Build\t\t: newlib"
echo $SEPARATOR
echo -e $DESC
echo -e "Source\t\t: $SRCDIR"
echo -e "Target\t\t: $TARGET"
echo -e "CWD\t\t: $CURDIR"
echo -e "CFLAGS\t\t: $CFLAGS"
echo -e "Version String\t: $TOOL_VERSION"
echo -e "Platform\t: $PLATFORM"

rm -rf $BLDDIR/build
rm -rf $BLDDIR/log
mkdir -p $BLDDIR/build
mkdir -p $BLDDIR/log

if [ ! -d $RELDIR ]; then mkdir -p $RELDIR; fi

errors=0

#  Create build directory
cd $BLDDIR/build

# Use back-rev gcc if available
if [ -d /usr/local/bin/gcc-3.4.6 ]; then
  PATH=/usr/local/bin/gcc-3.4.6/bin:$PATH
fi

PATH=$BINDIR:$PATH

#export AR_FOR_TARGET=${PROG_PREFIX}ar
#export RANLIB_FOR_TARGET=${PROG_PREFIX}ranlib
#export AS_FOR_TARGET=${PROG_PREFIX}as
#export CC_FOR_TARGET=${PROG_PREFIX}gcc
export AR_FOR_TARGET=mb-ar
export RANLIB_FOR_TARGET=mb-ranlib
export AS_FOR_TARGET=mb-as
export CC_FOR_TARGET=mb-gcc
echo -n "Configuring..."
$SRCDIR/configure 			\
    --target=$TARGET                    \
    --prefix=$RELDIR 			\
    --program-prefix=${PROG_PREFIX}	\
    --with-gnu-as 			\
    --with-gnu-ld 			\
    --enable-languages=$LANGS           \
    --disable-nls 			\
    --enable-multilib			\
    --enable-optimize-memory		\
    > $BLDDIR/log/newlib-config.log 2>&1 
    rc=$?
print_err " rc = " $rc
    #--disable-multilib			\
#--disable-newlib-atexit-dynamic-alloc	\

if [ $NOBUILD = 0 ]; then
  echo -n "Compiling..."
  $MAKE all  CFLAGS="$CFLAGS" CXXFLAGS="$CXXFLAGS" 	\
		> $BLDDIR/log/newlib-make.log 2>&1 
  rc=$?
  print_err " rc = " $rc

  echo -n "Installing..."
  $MAKE install CFLAGS="$CFLAGS" CXXFLAGS="$CXXFLAGS"	\
      > $BLDDIR/log/newlib-install.log 2>&1
  rc=$?
  print_err " rc = " $rc

#  echo "Building newlib variants..."
#  while read LIB NAME ; do
#      read FLAGS
#      make -C $BLDDIR/build/$TARGET/newlib/$LIB clean 	\
#	  >> $BLDDIR/log/libs-make.log 2>&1
#      make -C $BLDDIR/build/$TARGET/newlib/$LIB CFLAGS="$FLAGS" AR_FLAGS=rc   \
#	  >> $BLDDIR/log/libs-make.log 2>&1
#      print_err "   install $NAME -- rc = " $?
#      mv $BLDDIR/build/$TARGET/newlib/$LIB/$LIB.a $RELDIR/$TARGET/lib/$NAME   \
#	  >> $BLDDIR/log/libs-make.log 2>&1
#  done < $SRCDIR/$LIB_VARIANTS
fi

cd $CURDIR
#if [ $NOCLEAN = 0 ]; then
#  rm -rf $BLDDIR
#fi

echo "Build complete."
