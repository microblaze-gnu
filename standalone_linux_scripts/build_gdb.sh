#
#  Build GDB
#

#  Source and patches
CFLAGS="-g"

. common.sh

TARGET=microblaze-xilinx-elf
PROG_PREFIX=mb-

SRCDIR=$SRCDIR/../src/gdb
BLDDIR=$BLDDIR/bld_gdb
BINDIR=$RELDIR/bin

DESC="Build\t\t: gdb"
echo $SEPARATOR
echo -e $DESC
echo -e "Source\t\t: $SRCDIR"
echo -e "Target\t\t: $TARGET"
echo -e "CWD\t\t: $CURDIR"
echo -e "CFLAGS\t\t: $CFLAGS"
echo -e "Version String\t: $TOOL_VERSION"
echo -e "Platform\t: $PLATFORM"

rm -rf $BLDDIR/gdb
rm -rf $BLDDIR/build
rm -rf $BLDDIR/log
mkdir -p $BLDDIR/build
mkdir -p $BLDDIR/log

if [ ! -d $RELDIR ]; then mkdir -p $RELDIR; fi

# Set tool version
#cd $BLDDIR/gdb/gdb
#sed -e "s/\([0-9]*\.[0-9]*\.[0-9]*\).*$/\1 $TOOL_VERSION/" version.in > version.new
#mv version.in version.in.old
#mv version.new version.in

#  Create build directory
cd $BLDDIR/build

# Use back-rev gcc if available
if [ -d /usr/local/bin/gcc-3.4.6 ]; then
  PATH=/usr/local/bin/gcc-3.4.6/bin:$PATH
fi

PATH=$BINDIR:$PATH

echo -n "Configuring..."
$SRCDIR/configure 				\
	--target=$TARGET			\
	--prefix=$RELDIR 			\
	--with-python=no			\
	--program-prefix=$PROG_PREFIX		\
		> $BLDDIR/log/gdb-config.log 2>&1 
rc=$?
print_err " rc = " $rc

if [ $NOBUILD = 0 ]; then
  echo -n "Compiling..."
  $MAKE all -j CFLAGS="$CFLAGS"			\
		> $BLDDIR/log/gdb-make.log 2>&1
  rc=$?
  print_err " rc = " $rc
  
  echo -n "Installing..."
  $MAKE install CFLAGS="$CFLAGS" 		\
		> $BLDDIR/log/gdb-install.log 2>&1
  rc=$?
  print_err " rc = " $rc
fi

#cd $CURDIR
#if [ $NOCLEAN == 0 ]; then
#  rm -rf $BLDDIR
#fi

echo "Build complete."
