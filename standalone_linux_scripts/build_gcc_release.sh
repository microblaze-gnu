
#  Build Release GCC
#

LANGS="c,c++"
CFLAGS=""
CXXFLAGS=""

. ./common.sh

# Override source directory
SRCDIR=$SRCDIR/../src/gcc
BLDDIR=$BLDDIR/bld_gcc_release
BINDIR=$RELDIR/bin

DESC="Build\t\t: gcc release"
echo $SEPARATOR
echo -e $DESC
echo -e "Source\t\t: $SRCDIR"
echo -e "Build\t\t: $BLDDIR"
echo -e "Target\t\t: $TARGET"
echo -e "CWD\t\t: $CURDIR"
echo -e "CFLAGS\t\t: $CFLAGS"
echo -e "Version String\t: $TOOL_VERSION"
echo -e "Platform\t: $PLATFORM"

rm -rf $BLDDIR/build
rm -rf $BLDDIR/log
mkdir -p $BLDDIR/build
mkdir -p $BLDDIR/log

if [ ! -d $RELDIR ]; then mkdir -p $RELDIR; fi

errors=0

#  Create build directory
cd $BLDDIR/build

# Use back-rev gcc if available
if [ -d /usr/local/bin/gcc-3.4.6 ]; then
  PATH=/usr/local/bin/gcc-3.4.6/bin:$PATH
fi
if [ -e /usr/bin/gcc34 ]; then
  export CC=gcc34
fi

PATH=$BINDIR:$PATH

export AR_FOR_TARGET=${PROG_PREFIX}ar
export AS_FOR_TARGET=${PROG_PREFIX}as
echo -n "Configuring..."
touch $BLDDIR/log/gcc-config.log
$SRCDIR/configure			\
    --with-ppl=no		\
    --with-cloog=no	\
    --target=$TARGET			\
    --prefix=$RELDIR			\
    --program-prefix=$PROG_PREFIX 	\
    CFLAGS_FOR_TARGET=-O2		\
    CFLAGS_FOR_BUILD=-O2		\
    --with-newlib			\
    --enable-languages=$LANGS		\
    --enable-multilib			\
    --disable-libquadmath		\
    --disable-lto 			\
    --with-sysroot=$RELDIR/$TARGET		\
    > $BLDDIR/log/gcc-config.log 2>&1
    rc=$?
print_err " rc = " $rc

if [ $NOBUILD = 0 ]; then
  echo -n "Compiling..."
  $MAKE  all  	\
		> $BLDDIR/log/gcc-make.log 2>&1 
  rc=$?
  print_err " rc = " $rc

  echo -n "Installing..."
  $MAKE install 	\
      > $BLDDIR/log/gcc-install.log 2>&1
  rc=$?
  print_err " rc = " $rc
fi

cd $CURDIR
#if [ $NOCLEAN = 0 ]; then
#  rm -rf $BLDDIR
#fi

echo "Build complete."
