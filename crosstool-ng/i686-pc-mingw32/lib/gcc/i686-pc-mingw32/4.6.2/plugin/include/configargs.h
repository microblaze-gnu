/* Generated automatically. */
static const char configuration_arguments[] = "/proj/epdsw/nagaraj/linux/crosstool-ng/mingw32/.build/src/gcc-4.4.5/configure --build=i686-build_pc-linux-gnu --host=i686-build_pc-linux-gnu --target=i686-pc-mingw32 --prefix=/home/nmekala/x-tools/i686-pc-mingw32 --with-sysroot=/home/nmekala/x-tools/i686-pc-mingw32/i686-pc-mingw32/sysroot --enable-languages=c,c++ --with-arch=i686 --disable-shared --with-pkgversion='crosstool-NG 1.14.1' --enable-__cxa_atexit --disable-libmudflap --disable-libgomp --disable-libssp --with-gmp=/proj/epdsw/gnu/mb_gnu/libs/ --with-mpfr=/proj/epdsw/nagaraj/linux/crosstool-ng/mingw32/.build/i686-pc-mingw32/build/static --with-ppl=no --with-ppl=no --with-cloog=no --with-host-libstdcxx='-static-libgcc -Wl,-Bstatic,-lstdc++,-Bdynamic -lm' --enable-threads=win32 --disable-win32-registry --enable-target-optspace --enable-plugin --disable-nls --disable-multilib --with-local-prefix=/home/nmekala/x-tools/i686-pc-mingw32/i686-pc-mingw32/sysroot --enable-c99 --enable-long-long";
static const char thread_model[] = "win32";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "i686" } };
