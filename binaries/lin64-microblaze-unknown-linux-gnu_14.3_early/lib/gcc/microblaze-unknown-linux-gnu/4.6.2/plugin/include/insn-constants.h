/* Generated automatically by the program `genconstants'
   from the machine description file `md'.  */

#ifndef GCC_INSN_CONSTANTS_H
#define GCC_INSN_CONSTANTS_H

#define MB_PIPE_5 1
#define R_IR 14
#define MB_PIPE_3 0
#define R_GOT 20
#define R_TMP 18
#define R_DR 16
#define UNSPEC_GOTOFF 102
#define R_ER 17
#define UNSPEC_SET_GOT 101
#define UNSPEC_CMPU 105
#define R_SP 1
#define UNSPEC_PLT 103
#define R_SR 15
#define UNSPEC_TLS 106
#define UNSPEC_CMP 104

#endif /* GCC_INSN_CONSTANTS_H */
