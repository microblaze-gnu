#ifndef GCC_TM_H
#define GCC_TM_H
#ifndef LIBC_GLIBC
# define LIBC_GLIBC 1
#endif
#ifndef LIBC_UCLIBC
# define LIBC_UCLIBC 2
#endif
#ifndef LIBC_BIONIC
# define LIBC_BIONIC 3
#endif
#ifndef DEFAULT_LIBC
# define DEFAULT_LIBC LIBC_GLIBC
#endif
#ifndef ANDROID_DEFAULT
# define ANDROID_DEFAULT 0
#endif
#ifndef TARGET_BIG_ENDIAN_DEFAULT
# define TARGET_BIG_ENDIAN_DEFAULT 4321
#endif
#ifdef IN_GCC
# include "options.h"
# include "insn-constants.h"
# include "config/microblaze/microblaze.h"
# include "config/linux-android.h"
# include "config/dbxelf.h"
# include "config/gnu-user.h"
# include "config/linux.h"
# include "config/microblaze/linux.h"
# include "config/glibc-stdint.h"
#endif
#if defined IN_GCC && !defined GENERATOR_FILE && !defined USED_FOR_TARGET
# include "insn-flags.h"
#endif
# include "defaults.h"
#endif /* GCC_TM_H */
